#include "stm32f10x.h"
# include "adc.h"
#include "gpio.h"
#include "timer.h"
#include "audio.h"


/**
 * donne la phase actuelle du jeu
 * 0 : init
 * 1 : jouer la s�quence de LED
 * 2 : tour du joueur
 * 3 : changement de s�quence, retour en 1
*/
int gamePhase = 0;
int nbLife = 3;

/**
 * pour controler le TIM3 (allumage / extinction LED propre)
**/
int ON = 0;
int OFF = 0;

/**
 * sequence pour allumer les LED
 * premier element : nombre d'element total dans le tableau
 * 1 : LED bleu (BLU)
 * 2 : LED blanche (WHT)
 * 3 : LED verte (GRN)
 * 4 : LED rouge (RED)
*/
int seq1[5] = {4, 1, 2, 3, 4};
int seq2[6] = {5, 2, 2, 3, 4, 1};
int seq3[7] = {6, 1, 2, 3,1, 1, 2};
int seq4[8] = {7, 4, 3, 1, 2, 2, 4, 1};
int seq5[9] = {8, 2, 3, 4, 1, 3, 2, 2, 4};
int seq6[10] = {9, 3, 3, 3, 1, 4, 2, 4, 1, 1};
int seq7[11] = {10, 3, 1, 4, 2, 3, 4, 1, 2 ,4, 1};
int seq8[12] = {11, 4, 3, 4, 2, 1, 1, 2, 3, 4, 1, 2};
int seq9[13] = {12, 4, 1, 2, 3, 1, 2, 3, 4, 1, 2, 3, 4};
int seq10[14] = {13, 2, 3, 4, 1, 4, 4, 1 ,2 , 3, 1, 4, 2, 3};

int * seqTab[10] = {seq1, seq2, seq3, seq4, seq5, seq6, seq7, seq8, seq9, seq10};

int Input_check(int input, int T, int ite)
{
	if (input != seqTab[T][ite])
	{
		return 0;
	}
	else
	{
		return 1;
	}
}

void LED_reset()
{
	GPIOA->ODR &= ~(GPIO_ODR_ODR8);
	GPIOA->ODR &= ~(GPIO_ODR_ODR9);
	GPIOA->ODR &= ~(GPIO_ODR_ODR10);
	GPIOB->ODR &= ~(GPIO_ODR_ODR5);
}


/*
 * INPUT Pin pour Joystick
*/
GPIO_Struct A0 = {GPIOA, 0, In_Analog};
GPIO_Struct A1 = {GPIOA, 1, In_Analog};

/*
 * INPUT Pin pour Audio
*/
GPIO_Struct A2 = {GPIOA, 4, Out_Ppull};

/*
 * OUTPUT Pin pour LED
*/
GPIO_Struct D6 = {GPIOB, 10, Out_Ppull};
GPIO_Struct D4 = {GPIOB, 5, Out_Ppull};
GPIO_Struct D7 = {GPIOA, 8, Out_Ppull};
GPIO_Struct D8 = {GPIOA, 9, Out_Ppull};

LED_Struct WHT = {&D4, 0, 0};
LED_Struct BLU = {&D6, 0, 0};
LED_Struct GRN = {&D7, 0, 0};
LED_Struct RED = {&D8, 0, 0};

void TimerIT(void)
{
	if (gamePhase == 0)
	{
		if (ON)
		{
			GPIO_Reset(BLU.Port->GPIO, BLU.Port->GPIO_Pin);
			GPIO_Reset(WHT.Port->GPIO, WHT.Port->GPIO_Pin);
			GPIO_Reset(GRN.Port->GPIO, GRN.Port->GPIO_Pin);
			GPIO_Reset(RED.Port->GPIO, RED.Port->GPIO_Pin);
			ON = 0;
			gamePhase = 1;
		}
		else
		{
			GPIO_Set(BLU.Port->GPIO, BLU.Port->GPIO_Pin);
			GPIO_Set(WHT.Port->GPIO, WHT.Port->GPIO_Pin);
			GPIO_Set(GRN.Port->GPIO, GRN.Port->GPIO_Pin);
			GPIO_Set(RED.Port->GPIO, RED.Port->GPIO_Pin);
			ON = 1;
		}
	}
	if (gamePhase == 1)
	{
		if (ON)
		{
			if (BLU.isON)
			{
				GPIO_Reset(BLU.Port->GPIO, BLU.Port->GPIO_Pin);
				BLU.isON = 0;
				TIM3->PSC = 599;
				ON = 0;
				OFF = 1;
			}
		
			if (WHT.isON)
			{
				GPIO_Reset(WHT.Port->GPIO, WHT.Port->GPIO_Pin);	// allumage LED
				WHT.isON = 0;
				TIM3->PSC = 599;
				ON = 0;
				OFF = 1;
			}
		
			if (GRN.isON)
			{
				GPIO_Reset(GRN.Port->GPIO, GRN.Port->GPIO_Pin);	// allumage LED
				GRN.isON = 0;
				TIM3->PSC = 599;
				ON = 0;
				OFF = 1;
			}
		
			if (RED.isON)
			{
				GPIO_Reset(RED.Port->GPIO, RED.Port->GPIO_Pin);	// allumage LED
				RED.isON = 0;
				TIM3->PSC = 599;
				ON = 0;
				OFF = 1;
			}
		}
		else // si aucune LED n'est allum�, on reset le PSC pour 1s et on reset OFF
		{
			TIM3->PSC = 1199;
			OFF = 0;
		}
	}
	TIM3->SR &= ~TIM_SR_UIF;//(0x1 << 0);
}




void getSequence(int *seq, LED_Struct * LED1, LED_Struct * LED2, LED_Struct * LED3, LED_Struct * LED4)
{
	int i;
	//long j = 0;
	for (i = 1; i <= seq[0]; i++)
	{
		while ((LED1->isON) || (LED2->isON) || (LED3->isON) || (LED4->isON) ||(OFF)) {}
			
		switch(seq[i])
		{
			case 1:
				GPIO_Set(LED1->Port->GPIO, LED1->Port->GPIO_Pin);
				LED1->isON = 1;
				ON = 1;
				//for (j = 0; j < 7158278; j++) {}
				//GPIO_Reset(LED1->Port->GPIO, LED1->Port->GPIO_Pin);
				//for (j = 0; j < 3579139; j++) {}
			break;
			
			case 2:
				GPIO_Set(LED2->Port->GPIO, LED2->Port->GPIO_Pin);
				LED2->isON = 1;
				ON = 1;
				//for (j = 0; j < 7158278; j++) {}
				//GPIO_Reset(LED2->Port->GPIO, LED2->Port->GPIO_Pin);
				//for (j = 0; j < 3579139; j++) {}
				break;
			
			case 3:
				GPIO_Set(LED3->Port->GPIO, LED3->Port->GPIO_Pin);
				LED3->isON = 1;
				ON = 1;
				//for (j = 0; j < 7158278; j++) {}
				//GPIO_Reset(LED3->Port->GPIO, LED3->Port->GPIO_Pin);
				//for (j = 0; j < 3579139; j++) {}
				break;
			
			case 4:
				GPIO_Set(LED4->Port->GPIO, LED4->Port->GPIO_Pin);
				LED4->isON = 1;
				ON = 1;
				//for (j = 0; j < 7158278; j++) {}
				//GPIO_Reset(LED4->Port->GPIO, LED4->Port->GPIO_Pin);
				//for (j = 0; j < 3579139; j++) {}
				break;
			
			default:
				break;
		}
	}
}






int main (void)
{
	// ------------------------ INITIALISATION -------------------------
	// --- Init ADC ---
	GPIO_Init(&A0);
	ADC_Init();
	// ----------------
	
	// --- Init Son ---
	GPIO_Init(&A2);
	//-----------------
	
	// --- Init LED ---	
	GPIO_Init(&D6);
	GPIO_Init(&D4);
	GPIO_Init(&D7);
	GPIO_Init(&D8);
	LED_reset();
	// ----------------
	
	// --- Init TIM ---
	TimerInit();
	TimerInitIT(10);
	TimerInitRedirectionIT(TimerIT);
	TimerStart();
	// ----------------
	
	// -------------------------- GAME ---------------------------------
	int t = 0;
	
	while(gamePhase != -1)
	{
		while (gamePhase != 1) {};
		getSequence(seqTab[t], &BLU, &WHT, &GRN, &RED);
		LED_reset();
		gamePhase = 2;
			//Audio_play(0);
		//Audio_play(1);
		int k;
		short LED_select;
		int reussite;
		for (k=0; k <= seqTab[t][0]; k++)
		{
			LED_select = lecture_joystick();
			reussite = Input_check(LED_select, t, k);
			if (reussite)
			{
				// jouer son de winner
				t += 1;
			}
			else
			{
				//jouer son d'echec
				nbLife -= 1;
				if (nbLife <= 0)
				{
					gamePhase = -1;
					break;
				}
			}
		}
		/*if ( ! (GPIOD->ODR & (GPIO_ODR_ODR2)) )
		{
			GPIOA->ODR |= GPIO_ODR_ODR8;
			GPIOA->ODR |= GPIO_ODR_ODR9;
			GPIOB->ODR |= GPIO_ODR_ODR10;
			GPIOB->ODR |= GPIO_ODR_ODR5;
		}
		else
		{
			GPIOA->ODR &= ~(GPIO_ODR_ODR8);
			GPIOA->ODR &= ~(GPIO_ODR_ODR9);
			GPIOA->ODR &= ~(GPIO_ODR_ODR10);
			GPIOB->ODR &= ~(GPIO_ODR_ODR5);
		}*/
	}
	
	// jouer son de gameOver
	
	nbLife = 3;
}

