#ifndef __TIMER_H__
#define __TIMER_H__

#include "stm32f10x.h"

extern int gamePhase; 

void TimerInitRedirectionIT(void (* pointeur_Fonction) (void));
void TIM3_IRQHandler(void);
void TimerInit(void);
void TimerInitIT(int priority);
void TimerStart(void);
void TimerStop(TIM_TypeDef *timer);

#endif
