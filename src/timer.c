#include "timer.h"

void (*pFncTIM3) (void);

void TimerInitRedirectionIT(void (* pointeur_Fonction) (void))
{
	pFncTIM3 = pointeur_Fonction;	// affectation du pointeur
}


void TIM3_IRQHandler(void)
{
	if (pFncTIM3 != 0)
	{
		(*pFncTIM3) ();	// appel indirect a la fonction
	}
}


void TimerInitIT(int priority)
{
	TIM3->DIER = TIM_DIER_UIE;
	NVIC->ISER[0] = NVIC_ISER_SETENA_29;
	NVIC->IP[29] = (priority<<4);
}

void TimerInit(void)
{
	RCC->APB1ENR |= RCC_APB1ENR_TIM3EN;
	TIM3->ARR = 59999;
	TIM3->PSC = 1199;
}

void TimerStart(void)
{
	TIM3->CR1 = (1<<0);
}

/**
 Stop le timer
 @param timer Pointeur vers le jeu de registre de type TIM_TypeDef du timer considéré (TIM1, TIM2, TIM3 ou TIM4)
 */
void TimerStop(TIM_TypeDef *timer)
{
	timer->CR1 &= ~TIM_CR1_CEN;
	switch((int)timer)
	{
		case (int)TIM1:
			RCC->APB2ENR &= ~RCC_APB2ENR_TIM1EN;
			break;
		
		case (int)TIM2:
			RCC->APB1ENR &= ~RCC_APB1ENR_TIM2EN;
			break;
		
		case (int)TIM3:
			RCC->APB1ENR &= ~RCC_APB1ENR_TIM3EN;
			TIM3->CR1 &= ~TIM_CR1_CEN;
			break;
		
		case (int)TIM4:
			RCC->APB1ENR &= ~RCC_APB1ENR_TIM4EN;
			break;
		
		default:
			break;
	}
}


