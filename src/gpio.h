#ifndef __GPIO_H__
#define __GPIO_H__

#include "stm32f10x.h"

#define In_Floating 0x04
#define In_PullDown 0x08
#define In_PullUp 0x08
#define In_Analog 0x00
#define Out_Ppull 0x02
#define Out_OD 0x06
#define RESET 0xFF

typedef struct
{
	GPIO_TypeDef * GPIO;
	char GPIO_Pin;
	char GPIO_Techno;
}GPIO_Struct;


typedef struct
{
	GPIO_Struct * Port;
	int tick;	// init 0
	int isON; // init 0 (False)
}LED_Struct;


void GPIO_Init(GPIO_Struct * GPIOStructPtr);
int GPIO_Read(GPIO_TypeDef * GPIO, char GPIO_Pin);
void GPIO_Set(GPIO_TypeDef * GPIO, char GPIO_Pin);
void GPIO_Reset(GPIO_TypeDef * GPIO, char GPIO_Pin);

#endif
