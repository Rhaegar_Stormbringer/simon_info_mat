#include "adc.h"
 
/**
 * Init ADC pour la conversion du joystick
 **/
void ADC_Init(void)
	{
	// Configuration de l'horloge li�e � la grappe APB2, puis division de la fr�quence par defaut (72MHz) par 6, afin de la faire passer en-de�a du maximum de 14MHz
	RCC->APB2ENR |= RCC_APB2ENR_ADC1EN;
	RCC->CFGR |= RCC_CFGR_ADCPRE_DIV6;
	// Sortie du mode veille
	ADC1->CR2 |= ADC_CR2_ADON;
	// Mode discontinu
	ADC1->CR2 &= ~ADC_CR2_CONT;
		// Configuration du d�clenchement de l'acquisition par le syst�me (cf. SWSTART ci-dessous) : on met les trois bits de EXTSEL � 111.
	ADC1->CR2 |= ADC_CR2_EXTSEL;
	// Conversion "on external event enabled"
	ADC1->CR2 |= ADC_CR2_EXTTRIG;
	// D�finition du nombre total de conversions que l'ADC g�re en simultan� : 1 conversion.
	ADC1->SQR1 &= ~ADC_SQR1_L;
}


/**A_VERIFIER : On ateend ind�finiement la lev�e du flag ADC_SR_EOC.
 * Traduction de l'orientation du joystick en un entier (parcours cercle trigonom�trique pour la r�partition)
 * 1 si doite
 * 2 si haut
 * 3 si gauche
 * 4 si bas
 **/
short lecture_joystick(void)
{
	short sens = 0;
	// Axe de mesure (1 pour l'horizontal et 2 pour le vertical)
	short axe = 0;
	// <valeur> => short car ADC1_DR_DATA (registre contenant le r�sultat de la conversion) est stock�e sur 2 octets.
	short valeur_mesuree = 0;
	
	// Activation du d�clenchement conversions
	ADC1->CR2 |= ADC_CR2_SWSTART;
	
	// Alternance de l'axe mesur� jusqu'� ce qu'un seuil arbitraire soit d�pass�
	// Seuils d'acceptance de la mesure fix�s arbitrairement au tiers vers chaque sens (Output : 0 � 3,3V)
	while(1.1 < valeur_mesuree | valeur_mesuree < 2.2){
		if (axe == 1){
			ADC1->SQR3 |= 4;
			axe = 2;
		} else{
			ADC1->SQR3 |= 1;
			axe = 1;
		}
		// Attente fin conversion
		while( !(ADC1->SR & ADC_SR_EOC) );		
		// Stockage valeur convertie
		valeur_mesuree = ADC1->DR;
		// Validation conversion
		ADC1->SR &= ~ADC_SR_EOC;
	}
	
	// Assignation du sens en fonction de l'axe est de la valeur
	if (axe == 1){ // Axe horizontal
		if(valeur_mesuree > 0){
			// Droite
			sens = 1;
		} else{
			// Gauche
			sens = 3;
		}
	} else{ // Axe vertical	
		if(valeur_mesuree > 0){
			// Haut
			sens = 2;
		} else{
			// Bas
			sens = 4;
		}
	}

	return(sens);
}
