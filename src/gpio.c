#include "stm32f10x.h"
#include "gpio.h"

/* ---- */
void GPIO_Init(GPIO_Struct * GPIOPtr)
{
	if (GPIOPtr->GPIO == GPIOA)
	{
		RCC->APB2ENR |= RCC_APB2ENR_IOPAEN;
	}
	else if (GPIOPtr->GPIO == GPIOB)
	{
		RCC->APB2ENR |= RCC_APB2ENR_IOPBEN;
	}
	else if (GPIOPtr->GPIO == GPIOC)
	{
		RCC->APB2ENR |= RCC_APB2ENR_IOPCEN;
	}
	else
	{
		RCC->APB2ENR |= RCC_APB2ENR_IOPDEN;
	}
	
	int Pin = (int)GPIOPtr->GPIO_Pin;
	if (Pin < 8)
	{
		GPIOPtr->GPIO->CRL &= ~(RESET << Pin*4);
		GPIOPtr->GPIO->CRL |= (GPIOPtr->GPIO_Techno << Pin*4);
	}
	else
	{
		GPIOPtr->GPIO->CRH &= ~(RESET << (Pin % 8)*4);
		GPIOPtr->GPIO->CRH |= (GPIOPtr->GPIO_Techno << (Pin % 8)*4);
	}
}

/* ---- */
int GPIO_Read(GPIO_TypeDef * GPIO, char GPIO_Pin)
{
	return (int)((GPIO->IDR & (1 << GPIO_Pin)) >> GPIO_Pin);
}


void GPIO_Set(GPIO_TypeDef * GPIO, char GPIO_Pin)
{
	GPIO->ODR |= (0x1 << GPIO_Pin);
}


void GPIO_Reset(GPIO_TypeDef * GPIO, char GPIO_Pin)
{
	GPIO->ODR &= ~(0x1 << GPIO_Pin);
}
